package nz.co.hwankim;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static nz.co.hwankim.App.MAX_ALLOWED_CUSTOMER_BOOKING_PER_HOUR;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * -- Acceptance Criteria: --
 * A time slot can be booked by up to 8 users at the same time
 * One user can only book one-time slot per day
 * One user can book one-time slot every day
 * 1. Report all booked time slots for a given user-id
 * Input: userId
 * Output: Sequence of time slots (day, hour)
 * 1. Time slot utilization report for a given day: day Sequence of (hour count) pairs
 * Input: day (Monday, Tuesday, ...)
 * Output: a sequence or list of pairs (hour, #bookings) ordered by hour ascending
 */
public class AppTest {
    private App testTarget = new App();

    @Before
    public void beforeTest() {
        testTarget.reset();
    }

    @Test
    public void bookTimeSlot() {
        String userId = "userId";
        App.DayOfWeek testDay = App.DayOfWeek.Fri;
        assertTrue(testTarget.bookTimeSlot(testDay, 1, userId));

        //Verify user Booking
        Set<String> testResult = testTarget.getBookedUserIds(testDay, 1);
        assertThat(testResult.size(), is(1));
        assertTrue(testResult.contains(userId));
    }

    @Test
    public void bookTimeSlot_MaxBookingCustomerReached() {
        String userIdFormat = "userID_%d";
        for (int i = 0; i < MAX_ALLOWED_CUSTOMER_BOOKING_PER_HOUR; i++)
            assertTrue(testTarget.bookTimeSlot(App.DayOfWeek.Mon, 1, String.format(userIdFormat, i)));
        assertFalse("A time slot can be booked by up to 8 users at the same time",
                testTarget.bookTimeSlot(App.DayOfWeek.Mon, 1, "lateBookingCustomer"));

        //Verify user booking.
        Set<String> bookedUserIds = testTarget.getBookedUserIds(App.DayOfWeek.Mon, 1);
        AtomicInteger userIdOffset = new AtomicInteger(0);
        bookedUserIds.forEach(userId -> assertThat(userId, is(String.format(userIdFormat, userIdOffset.getAndIncrement()))));
    }

    @Test
    public void bookTimeSlot_OneTimeSlot_EveryDay() {
        String userId = "bookingEveryDayOnce";
        Arrays.stream(App.DayOfWeek.values()).forEach(day ->
                assertTrue("One user can book one-time slot every day",
                        testTarget.bookTimeSlot(day, 1, userId))
        );

        //Verify user booking.
        Arrays.stream(App.DayOfWeek.values()).forEach(day -> {
            Optional<String> testResult = testTarget.getBookedUserIds(day, 1).stream().findFirst();
            assertTrue(testResult.isPresent());
            assertThat(testResult.get(), is(userId));
        });
    }

    @Test
    public void bookTimeSlot_BookedAlready_SameDay() {
        String userId = "bookingMonday";
        App.DayOfWeek testDay = App.DayOfWeek.Mon;
        assertTrue(testTarget.bookTimeSlot(testDay, 1, userId));
        assertFalse("One user can only book one-time slot per day",
                testTarget.bookTimeSlot(testDay, 2, userId));

        //Verify user Booking
        Set<String> testResult = testTarget.getBookedUserIds(testDay, 1);
        assertThat(testResult.size(), is(1));
        assertTrue(testResult.contains(userId));
    }

    @Test
    public void reportBookedCustomer() {
        final String userIDFormat = "CID_%d_%d";
        final App.DayOfWeek[] testBookedDays = {App.DayOfWeek.Mon, App.DayOfWeek.Sat};
        //Test data setup.
        for (App.DayOfWeek day : testBookedDays)
            for (int hour = 0; hour < 24; hour++)
                for (int bookedCustomerNo = 0; bookedCustomerNo < MAX_ALLOWED_CUSTOMER_BOOKING_PER_HOUR; bookedCustomerNo++)
                    assertTrue(testTarget.bookTimeSlot(day, hour, String.format(userIDFormat, bookedCustomerNo, hour)));
        // Trigger test
        List<Pair<Integer, Set<String>>> testResult = testTarget.reportBookedUsers(testBookedDays);
        //Check ordered by hour ascending
        for (int hour = 0; hour < 24; hour++) {
            Pair<Integer, Set<String>> bookedCustomerInTimeSlot = testResult.get(hour);
            assertTrue(bookedCustomerInTimeSlot.getKey() == hour);
            //Checking customer added above test are returned.
            for (int bookedCustomerNo = 0; bookedCustomerNo < MAX_ALLOWED_CUSTOMER_BOOKING_PER_HOUR; bookedCustomerNo++) {
                String expectedID = String.format(userIDFormat, bookedCustomerNo, hour);
                assertTrue("Expect to find userID:" + expectedID + " at time:" + hour,
                        bookedCustomerInTimeSlot.getValue().contains(expectedID));
            }
        }
    }
}
