package nz.co.hwankim;

import javafx.util.Pair;

import java.util.*;

public class App {
    public enum DayOfWeek {
        Mon, Tue, Wed, Thu, Fri, Sat, Sun
    }

    /**
     * Simplified version of data source.
     */
    private final static Map<DayOfWeek /*Day of Week*/,
            Map<Integer /*Hour of day*/, Set<String> /*Booked Customer IDs*/>> weeklyCustomerBookableTimeSlots =
            new HashMap<>();
    protected final static int MAX_ALLOWED_CUSTOMER_BOOKING_PER_HOUR = 8, START_HOUR_OF_DAY = 0, END_HOUR_OF_DAY = 24;

    protected Set<String> getBookedUserIds(final DayOfWeek day, final int hourOfDay) {
        return weeklyCustomerBookableTimeSlots.get(day).getOrDefault(hourOfDay, new HashSet<>());
    }

    /**
     * Book a time slot given a user ID, a day and an hour
     *
     * @param day
     * @param hourOfDay
     * @param userId
     * @return true if it could be booked successfully or false
     */
    public boolean bookTimeSlot(final DayOfWeek day, final int hourOfDay, final String userId) {
        // A time slot can be booked by up to 8 users at the same time
        if (getBookedUserIds(day, hourOfDay).size() >= MAX_ALLOWED_CUSTOMER_BOOKING_PER_HOUR)
            return false;
        // One user can only book one-time slot per day
        if (reportBookedTimeSlots(userId).stream().anyMatch(timeSlot -> timeSlot.getKey() == day))
            return false;
        book(day, hourOfDay, userId);
        return true;
    }

    // Report all booked time slots for a given user-id
    public List<Pair<DayOfWeek, Integer>> reportBookedTimeSlots(final String userId) {
        List<Pair<DayOfWeek, Integer>> bookedTimeSlots = new ArrayList<>();
        for (DayOfWeek day : DayOfWeek.values()) {
            for (int hourOfDay = START_HOUR_OF_DAY; hourOfDay < END_HOUR_OF_DAY; hourOfDay++) {
                if (getBookedUserIds(day, hourOfDay).contains(userId)) {
                    bookedTimeSlots.add(new Pair<>(day, hourOfDay));
                }
            }
        }
        return bookedTimeSlots;
    }

    // Time slot utilization report for a given day: day Sequence of (hour count) pairs
    public List<Pair<Integer, Set<String>>> reportBookedUsers(final DayOfWeek... days) {
        List<Pair<Integer, Set<String>>> bookedUserIds = new ArrayList<>();
        for (DayOfWeek day : days)
            for (int hourOfDay = START_HOUR_OF_DAY; hourOfDay < END_HOUR_OF_DAY; hourOfDay++)
                bookedUserIds.add(new Pair<>(hourOfDay, getBookedUserIds(day, hourOfDay)));

        return bookedUserIds;
    }


    /**
     * Test support method.
     */
    protected void reset() {
        weeklyCustomerBookableTimeSlots.clear();
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            weeklyCustomerBookableTimeSlots.put(dayOfWeek, new LinkedHashMap<>());
            for (int hourOfDay = START_HOUR_OF_DAY; hourOfDay < END_HOUR_OF_DAY; hourOfDay++) {
                weeklyCustomerBookableTimeSlots.get(dayOfWeek).put(hourOfDay, new LinkedHashSet<>());
            }
        }
    }

    private void book(final DayOfWeek day, final int hourOfDay, final String userId) {
        if (!weeklyCustomerBookableTimeSlots.containsKey(day))
            weeklyCustomerBookableTimeSlots.put(day, new LinkedHashMap<>());
        if (!weeklyCustomerBookableTimeSlots.get(day).containsKey(hourOfDay))
            weeklyCustomerBookableTimeSlots.get(day).put(hourOfDay, new LinkedHashSet<>());

        weeklyCustomerBookableTimeSlots.get(day).get(hourOfDay).add(userId);
    }
}
